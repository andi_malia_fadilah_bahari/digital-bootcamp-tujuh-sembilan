package childs;

import parent.Person;

public class Doctor extends Person {
	private String specialist;
	
	public Doctor() {
		super();
	}
	
	public Doctor(String name, String address, String specialist) {
		super(name, address);
		this.specialist = specialist;
	}



	void surgery() {
		System.out.println("I can surgery operation Patients");
	}

	@Override
	public void greeting() {
		super.greeting();
		System.out.println("My occupation is a "+specialist+" doctor.");
	}
	

	//Getter
	public String getSpecialist() {
		return this.specialist;
	}
	//setter
	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
}