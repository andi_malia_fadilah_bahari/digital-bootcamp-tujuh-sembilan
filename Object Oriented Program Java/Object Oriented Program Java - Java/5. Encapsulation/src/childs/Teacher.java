package childs;

import parent.Person;

public class Teacher extends Person {
	private String subject;

	public Teacher() {
		super();
	}
	
	
	public Teacher(String name, String address, String subject) {
		super(name, address);
		this.subject = subject;
	}
	
	
	void teaching() {
		System.out.println("I can tach "+subject+", So anyone how wants to learn can talk to me.");
	}

	@Override
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+subject+" teacher.");
	}
	

	//Getter
	public String getSubject() {
		return this.subject;
	}
	//setter
	public void setSubject(String subject) {
		this.subject = subject;
	}
}
