package parent;


public class Person {
	private String name;
	private String address;

	public Person(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}

	public Person() {
		super();
	}
	
	public void greeting() {
		System.out.println("Hello my name is "+name+".");
		System.out.println("I, come from "+address+".");
	}


	//Getter
	public String getName() {
		return this.name;
	}
	//setter
	public void setName(String name) {
		this.name = name;
	}

	//Getter
	public String getAddress() {
		return this.address;
	}
	//setter
	public void setAddress(String address) {
		this.address = address;
	}
}
