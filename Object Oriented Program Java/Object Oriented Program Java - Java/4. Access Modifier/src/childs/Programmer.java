package childs;
import parent.Person;

public class Programmer extends Person {
	public String technology;
	
	public Programmer() {
		super();
	}
	
	public Programmer(String name, String address, String technology) {
		super(name, address);
		this.technology = technology;
	}

	void hacking() {
		System.out.println("I can hacking a website");
	}
	
	void coding() {
		System.out.println("I can create application using technology: "+technology+".");
	}
	
	@Override
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+technology+" programmer.");
	}
}
