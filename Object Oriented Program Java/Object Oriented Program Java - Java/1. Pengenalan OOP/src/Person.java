
public class Person {
	String name;
	String address;
	final String country = "Indonesia";
	
	
	//Constructur Default
	Person() {
		
	}
	
	//Constructor dengan 1 Parameter
	Person(String paramName){
		name = paramName;
	}
	
	//Constructor dengan Parameter
	Person(String paramName, String paramAddress){
		this(paramName);
		address = paramAddress;
	}
	
	
	void sayHello(String paramName) {
		System.out.println("Hello "+paramName+", My name is "+name+".");
	}
	
	String sayAddress() {
		return "I, come from "+address+".";
	}
}
