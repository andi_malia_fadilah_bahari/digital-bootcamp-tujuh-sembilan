
public class MainApp {

	public static void main(String[] args) {
		Person person1 = new Person();
		person1.name = "Eko";
		person1.address = "Tegal";

		System.out.println(person1.name);
		System.out.println(person1.address);
		System.out.println(person1.country);
	
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
	
		//Membuat object dari calls Person menggunakan constructor Parameter
		Person person2 = new Person("Joko","Surabaya");
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());

		
		//Membuat object dari calls Person menggunakan constructor 1 Parameter
		Person person3 = new Person("Budi");
		person3.address = "Bandung";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
		
	}

}
