import parents.Product;
import parents.Shape;
import childs.Item;
import childs.Circle;
import childs.Triangle;

public class MainApp {

	public static void main(String[] args) {
		AbstractClass();
		ShapeProject ();
	}
	
	static void AbstractClass() {
		Product product1 = new Product();
		Product product2 = new Item("Laptop",3000000);

		product1.printInformation();
		System.out.println();
		product2.printInformation();
	}
	
	static void ShapeProject () {
		Shape lingkaran = new Circle(20,"Biru");
		Shape segitiga = new Triangle(10,15,"Merah");

		System.out.println("Luas Lingkaran berwarna "+lingkaran.getColor()+" adalah "+lingkaran.getArea());
		System.out.println("Luas Segitiga berwarna "+segitiga.getColor()+" adalah "+segitiga.getArea());
	}

}
