import java.util.Scanner;

public class JobVacancy {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		String name,gender,school,job;
		int age;
		double exp,looks;
		boolean qualify;
		
		System.out.println("Silahkan input Data Anda");
		System.out.print("Nama: ");
		name = keyboard.next();
		System.out.print("Umur: ");
		age = keyboard.nextInt();
		System.out.print("Jenis Kelamin (Pria/Wanita): ");
		gender = keyboard.next();
		System.out.print("Lulusan (SMK/D3/S1): ");
		school = keyboard.next();
		System.out.print("Pengalaman (Tahun): ");
		exp = keyboard.nextDouble();
		System.out.print("Penampilan (0.0 - 10.0): ");
		looks = keyboard.nextDouble();
		System.out.print("Posisi yang dilamar (SPV, Admin, atau Koordinator): ");
		job = keyboard.next();
		
		System.out.println(name+", Terima kasih sudah mengikuti Lowongan Kerja di PT, Secret Semut 79.");
		System.out.println();
		
		qualify = qualification(job, gender, age, school, exp, looks);
		result(name,job,qualify);
	}
	
	public static boolean qualification(String job, String gender, int age, String school, double exp, double looks) {
		boolean value = false;
		if (job.equals("Koordinator")){
			System.out.println("Berikut adalah hasil dari Rekrutment untuk Koordinator: ");
			if ((gender.equals("Pria")) && (age > 20 && age <= 30) && (school.equals("SMK")||school.equals("D3")) && (exp >= 2)){value = true;}
			else if ((gender.equals("Pria")) && (age > 30) && (school.equals("S1")) && (exp >= 5)) {value = true;}
			else { value = false; }
		}
		else if (job.equals("Admin")){
			System.out.println("Berikut adalah hasil dari Rekrutment untuk Admin: ");
			if ((gender.equals("Wanita")) && (age >= 20 && age <= 25) && (school.equals("D3")) && (exp >= 1) && (looks >= 8.5)){ value = true; }
			else if ((gender.equals("Wanita")) && (age > 25) && (school.equals("S1")) && (exp >= 3) && (looks >= 8.5)) { value = true;}
			else if ((gender.equals("Pria")) && (age >= 20 && age <= 30) && (school.equals("D3")) && (exp >= 2) && (looks >= 8.5)) { value = true; }
			else { value = false; }
		}
		else if (job.equals("SPV")){
			if ((age >= 23 && age <= 30) && (school.equals("S1")) && (exp > 1)){ value = true; }
			else if ((age >= 25 && age <= 35) && (school.equals("D3")) && (exp > 4)) {value = true;}
			else {value = false;}
		}
		return value;
	}
	
	public static void result(String name, String job, boolean lulus) {
		if(lulus) {
			System.out.println("Selamat, "+name);
			System.out.println("Anda memenuhi syarat sebagai "+job+". Dan akan masuk ke Tahap Selanjutnya");
		}
		else {
			System.out.println("Maaf, "+name);
			System.out.println("Anda Tidak memenuhi syarat sebagai "+job+". Dan Tidak akan masuk ke Tahap Selanjutnya");
		}
	}
}