import java.util.Scanner;
import java.util.Random;

public class PigDice {
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
		Random rand = new Random();
		
		int player,computer,dice, round;
		boolean playerOn, winner, hold;
		String answer;
		player = 0;
		computer = 0;
		round = 1;
		winner = false;
		
		while(!winner){
			System.out.println("Round "+round);
			
			dice = 0;
			//Computer Play
			playerOn = false;
			System.out.println("Computer has "+computer+" Point");
			while(!playerOn && dice != 1){
				dice = rand.nextInt(6-1)+1;
				System.out.println("Computer rolled a "+dice);
				if(dice != 1) {
					computer = computer + dice;
					System.out.println("Computer has "+computer+" point so far this round");
					if(rand.nextInt(1)==1){
						System.out.println("Computer will roll again");
						hold = false;
					}
					else{ 
						System.out.println("Computer choose Hold");
						break;
					}
				}
			}
			System.out.println("Computer Ended the round with "+computer+" point");
			
			if(computer >= 50) {
				winner = true;
				break;
			}
			
			dice = 0;
			//Player Play
			playerOn = true;
			System.out.println("\nPlayer has "+player+" Point");
			while(playerOn && dice != 1){
				dice = rand.nextInt(6-1)+1;
				System.out.println("Player rolled a "+dice);
				if(dice != 1) {
					player = player + dice;
					System.out.println("Player has "+player+" point so far this round");
					System.out.print("Would you like to 'roll' again or 'hold'?");
					answer = input.next();
					if(answer.equals("roll")){
						System.out.println("Player will roll again");
					}
					else{ break; }
				}
			}
			System.out.println("Player Ended the round with "+player+" point");
			
			if(player >= 50 || computer >= 50) {winner = true;}
			round++;
			System.out.println();
		}
		if(player > computer) { System.out.println("Humanity Win!!");}
		else if(player < computer) { System.out.println("Computer Win!!");}
		else if(player == computer) { System.out.println("DRAW");}
	}
}