import java.util.Scanner;

public class PassingGradeSMK79 {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		double mtk,indo,ing,ipa,kkm_smk,kkm_bahasa,kkm_kom;
		double avg,avg_bahasa;
		kkm_smk = 73.0;
		kkm_bahasa = 75.0;
		kkm_kom = 80.0;
		
		// kkm_smk = rata-rata seluruh ujian >= 73
		// kkm_bahasa = rata-rata indo DAN ing >= 75
		// kkm_kom = mtk > 80
		
		System.out.print("Masukan nilai ujian Matematika: ");
		mtk = keyboard.nextDouble();
		
		System.out.print("Masukan nilai ujian Bhs. Indonesia: ");
		indo = keyboard.nextDouble();
		
		System.out.print("Masukan nilai ujian Bhs. Inggris: ");
		ing = keyboard.nextDouble();
		
		System.out.print("Masukan nilai ujian IPA: ");
		ipa = keyboard.nextDouble();
		
		avg = (mtk + indo + ing + ipa)/4;
		avg_bahasa = (indo + ing)/2;
		
		System.out.println("Apakah anda memenuhi Syarat untuk Masuk ke SMK Padepokan 79: "+(avg >=kkm_smk));
		System.out.println("Apakah anda memenuhi Syarat untuk Masuk ke Jurusan Sastra: "+(avg_bahasa >=kkm_bahasa));
		System.out.println("Apakah anda memenuhi Syarat untuk Masuk ke Jurusan Teknik Komputer Jaringan: "+(mtk > kkm_kom));
	}
}