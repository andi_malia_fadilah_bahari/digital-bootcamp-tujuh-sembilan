import java.util.Scanner;

public class JobVacantion {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		String name,gender,school;
		int age;
		double exp,looks;
		boolean koor,admin;
		
		System.out.println("Silahkan input Data Anda");
		System.out.print("Nama: ");
		name = keyboard.next();
		System.out.print("Umur: ");
		age = keyboard.nextInt();
		System.out.print("Jenis Kelamin (Pria/Wanita): ");
		gender = keyboard.next();
		System.out.print("Lulusan (SMK/D3/S1): ");
		school = keyboard.next();
		System.out.print("Pengalaman (Tahun): ");
		exp = keyboard.nextDouble();
		System.out.print("Penampilan (0.0 - 10.0): ");
		looks = keyboard.nextDouble();
		
		koor = ( ((gender.equals("Pria")) && (age >= 21 && age <= 30) && (school.equals("SMK")||school.equals("D3")) && (exp >= 2)) || ((gender.equals("Pria")) && (age > 30) && (school.equals("S1")) && (exp >= 5)));
		admin = ( ((gender.equals("Wanita")) && (age >= 20 && age <= 25) && (school.equals("D3")) && (exp >= 1) && (looks > 7.5)) || ((gender.equals("Pria")) && (age > 25) && (school.equals("S1")) && (exp >= 5) && (looks > 7.5)));
		
		System.out.println(name+", Terima kasih sudah mengikuti Lowongan Kerja di PT, Secret Semut 79.");
		System.out.println();
		System.out.println("Berikut adalah hasil dari Rekrutment untuk Koordinator dan Admin: ");
		System.out.println("Hasil Kelulusan untuk Koordinator: "+koor);
		System.out.println("Hasil Kelulusan untuk Admin: "+admin);
	}
}