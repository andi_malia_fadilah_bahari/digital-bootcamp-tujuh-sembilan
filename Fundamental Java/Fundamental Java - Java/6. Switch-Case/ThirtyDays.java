import java.util.Scanner;

public class ThirtyDays{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int month, days;
		String montName;
		
		System.out.print("Wich month? (1-12) ");
		month = input.nextInt();
		
		switch(month){
			case 1:
				montName = "January";
				break;
			case 2:
				montName = "February";
				break;
			case 3:
				montName = "March";
				break;
			case 4:
				montName = "April";
				break;
			case 5:
				montName = "May";
				break;
			case 6:
				montName = "June";
				break;
			case 7:
				montName = "July";
				break;
			case 8:
				montName = "August";
				break;
			case 9:
				montName = "September";
				break;
			case 10:
				montName = "October";
				break;
			case 11:
				montName = "November";
				break;
			case 12:
				montName = "December";
				break;
			default:
				montName = "Error";
				break;
		}
		
		switch(month){
			case 4:
			case 6:
			case 9:
			case 11:
				days = 30;
				break;
			case 2:
				days = 28;
				break;
			default:
				days = 31;
				break;
		}
		
		System.out.println(days+" Days Hath "+montName);
	}
}