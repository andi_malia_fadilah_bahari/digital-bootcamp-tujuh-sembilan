import java.util.Scanner;

public class JobVacancy {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		String name,gender,school,job;
		int age;
		double exp,looks;
		
		System.out.println("Silahkan input Data Anda");
		System.out.print("Nama: ");
		name = keyboard.next();
		System.out.print("Umur: ");
		age = keyboard.nextInt();
		System.out.print("Jenis Kelamin (Pria/Wanita): ");
		gender = keyboard.next();
		System.out.print("Lulusan (SMK/D3/S1): ");
		school = keyboard.next();
		System.out.print("Pengalaman (Tahun): ");
		exp = keyboard.nextDouble();
		System.out.print("Penampilan (0.0 - 10.0): ");
		looks = keyboard.nextDouble();
		System.out.print("Posisi yang dilamar (SPV, Admin, atau Koordinator): ");
		job = keyboard.next();
		
		System.out.println(name+", Terima kasih sudah mengikuti Lowongan Kerja di PT, Secret Semut 79.");
		System.out.println();
		
		if (job.equals("Koordinator")){
			System.out.println("Berikut adalah hasil dari Rekrutment untuk Koordinator: ");
			if ((gender.equals("Pria")) && (age > 20 && age <= 30) && (school.equals("SMK")||school.equals("D3")) && (exp >= 2)){
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai Koordinator. Dan akan masuk ke Tahap Selanjutnya");
			}
			else if ((gender.equals("Pria")) && (age > 30) && (school.equals("S1")) && (exp >= 5)) {
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai Koordinator. Dan akan masuk ke Tahap Selanjutnya");
			}
			else {
				System.out.println("Maaf, "+name);
				System.out.println("Anda Tidak memenuhi syarat sebagai Koordinator. Dan Tidak akan masuk ke Tahap Selanjutnya");
			}
		}
		else if (job.equals("Admin")){
			System.out.println("Berikut adalah hasil dari Rekrutment untuk Admin: ");
			if ((gender.equals("Wanita")) && (age >= 20 && age <= 25) && (school.equals("D3")) && (exp >= 1) && (looks >= 8.5)){
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai Admin. Dan akan masuk ke Tahap Selanjutnya");
			}
			else if ((gender.equals("Wanita")) && (age > 25) && (school.equals("S1")) && (exp >= 3) && (looks >= 8.5)) {
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai Admin. Dan akan masuk ke Tahap Selanjutnya");
			}
			else if ((gender.equals("Pria")) && (age >= 20 && age <= 30) && (school.equals("D3")) && (exp >= 2) && (looks >= 8.5)) {
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai Admin. Dan akan masuk ke Tahap Selanjutnya");
			}
			else {
				System.out.println("Maaf, "+name);
				System.out.println("Anda Tidak memenuhi syarat sebagai Admin. Dan Tidak akan masuk ke Tahap Selanjutnya");
			}
		}
		else if (job.equals("SPV")){
			if ((age >= 23 && age <= 30) && (school.equals("S1")) && (exp > 1)){
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai SPV. Dan akan masuk ke Tahap Selanjutnya");
			}
			else if ((age >= 25 && age <= 35) && (school.equals("D3")) && (exp > 4)) {
				System.out.println("Selamat, "+name);
				System.out.println("Anda memenuhi syarat sebagai SPV. Dan akan masuk ke Tahap Selanjutnya");
			}
			else {
				System.out.println("Maaf, "+name);
				System.out.println("Anda Tidak memenuhi syarat sebagai SPV. Dan Tidak akan masuk ke Tahap Selanjutnya");
			}
		}
	}
}