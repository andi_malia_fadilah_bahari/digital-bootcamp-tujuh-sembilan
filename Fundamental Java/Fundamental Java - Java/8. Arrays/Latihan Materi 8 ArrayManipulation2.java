import java.util.Scanner;

public class ArrayManipulation2 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		int[] arr = {3,2,5,11,7,10,11,3,15,11,17,10,5};
		int pos,nvalue;
		
		for(int d:arr){System.out.print(d+" ");}
		
		System.out.println("\nMerubah nilai dari arrays numbers !!!");
		System.out.print("Masukan posisi yang mau diganti 1 - 13: ");
		pos = input.nextInt();
		System.out.print("Masukan nilai yang akan diinputkan kedalam array numbers: ");
		nvalue = input.nextInt();
		
		System.out.println("Posisi "+pos+" adalah angka '"+arr[pos]+"'. akan digantikan oleh "+nvalue+".");
		arr[pos] = nvalue;
		System.out.println("Menampilkan array numbers setelah dirubah");
		for(int d:arr){System.out.print(d+" ");}
	}
}